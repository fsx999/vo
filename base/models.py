# coding=utf-8
from distutils.version import LooseVersion
import re
from cms.models import CMSPlugin
from cms.models.fields import PageField
from cms.plugins.googlemap.models import GoogleMap
from cmsplugin_filer_image.models import FilerImage, ThumbnailOption
from cmsplugin_filer_utils import FilerPluginManager
import django
from django.db import models
from django.utils.html import strip_tags, strip_spaces_between_tags
from django.utils.translation import ugettext_lazy as _
# Create your models here.
from filer.fields.file import FilerFileField
from filer.fields.image import FilerImageField


class MonFilerImage(CMSPlugin):
    LEFT = "left"
    RIGHT = "right"
    CENTER = "center"
    FLOAT_CHOICES = ((LEFT, _("left")),
                     (RIGHT, _("right"),),
                     (CENTER, _("center")),)

    caption_text = models.CharField(_("caption text"), null=True, blank=True, max_length=255)
    image = FilerImageField(null=True, blank=True, default=None, verbose_name=_("image"))
    if LooseVersion(django.get_version()) < LooseVersion('1.5'):
        image_url = models.URLField(_("alternative image url"), null=True, blank=True, default=None)
    else:
        image_url = models.URLField(_("alternative image url"), null=True, blank=True, default=None)
    alt_text = models.CharField(_("alt text"), null=True, blank=True, max_length=255)
    use_original_image = models.BooleanField(_("use the original image"), default=False,
        help_text=_('do not resize the image. use the original image instead.'))
    thumbnail_option = models.ForeignKey(ThumbnailOption, null=True, blank=True, verbose_name=_("thumbnail option"),
                                        help_text=_('overrides width, height, crop and upscale with values from the selected thumbnail option'))
    use_autoscale = models.BooleanField(_("use automatic scaling"), default=False,
                                        help_text=_('tries to auto scale the image based on the placeholder context'))
    width = models.PositiveIntegerField(_("width"), null=True, blank=True)
    height = models.PositiveIntegerField(_("height"), null=True, blank=True)
    crop = models.BooleanField(_("crop"), default=True)
    upscale = models.BooleanField(_("upscale"), default=True)
    alignment = models.CharField(_("image alignment"), max_length=10, blank=True, null=True, choices=FLOAT_CHOICES)

    free_link = models.CharField(_("link"), max_length=255, blank=True, null=True,
                                 help_text=_("if present image will be clickable"))
    page_link = PageField(null=True, blank=True,
                          help_text=_("if present image will be clickable"),
                          verbose_name=_("page link"))
    file_link = FilerFileField(null=True, blank=True, default=None, verbose_name=_("file link"), help_text=_("if present image will be clickable"), related_name='+')
    original_link = models.BooleanField(_("link original image"), default=False, help_text=_("if present image will be clickable"))
    description = models.TextField(_("description"), blank=True, null=True)
    target_blank = models.BooleanField(_('Open link in new window'), default=False)

    # we only add the image to select_related. page_link and file_link are FKs
    # as well, but they are not used often enough to warrant the impact of two
    # additional LEFT OUTER JOINs.
    objects = FilerPluginManager(select_related=('image',))


    class Meta:
        verbose_name = _("filer image")
        verbose_name_plural = _("filer images")

    def clean(self):
        from django.core.exceptions import ValidationError
        # Make sure that either image or image_url is set
        if (not self.image and not self.image_url) or (self.image and self.image_url):
            raise ValidationError(_('Either an image or an image url must be selected.'))


    def __unicode__(self):
        if self.image:
            return self.image.label
        else:
            return unicode( _("Image Publication %(caption)s") % {'caption': self.caption or self.alt} )
        return ''
    @property
    def caption(self):
        if self.image:
            return self.caption_text or self.image.default_caption
        else:
            return self.caption_text
    @property
    def alt(self):
        if self.image:
            return self.alt_text or self.image.default_alt_text or self.image.label
        else:
            return self.alt_text
    @property
    def link(self):
        if self.free_link:
            return self.free_link
        elif self.page_link:
            return self.page_link.get_absolute_url()
        elif self.file_link:
            return self.file_link.url
        elif self.original_link:
            if self.image:
                return self.image.url
            else:
                return self.image_url
        else:
            return ''


class Professeur(models.Model):
    nom = models.CharField(u'Nom', max_length=50)
    prenom = models.CharField(u'Prénom', max_length=50)
    date_naissance = models.DateField(u"Date de naissance", null=True, blank=True)
    date_creation = models.DateTimeField(auto_now_add=True, editable=False)
    date_update = models.DateTimeField(auto_now=True, auto_now_add=True, editable=False)
    email = models.EmailField(null=True, blank=True)
    tel = models.CharField(null=True, max_length="10", verbose_name=u"Téléphone", blank=True)

    def __unicode__(self):
        return u"%s %s" % (self.nom, self.prenom)

    class Meta:
        verbose_name = u"Professeur"
        verbose_name_plural = u"Professeurs"


class ListeProfesseur(CMSPlugin):
    pass

class ListeSalle(CMSPlugin):
    pass

class Salle(models.Model):
    nom = models.CharField(u'Nom', max_length=50)
    professeurs = models.ManyToManyField(Professeur, null=True, blank=True)
    horaire = models.TextField(u"Horaire", null=True, blank=True)
    adresse = models.CharField(u"adresse", max_length=500, null=True, blank=True)
    description = models.TextField(u'description', null=True, blank=True)
    date_creation = models.DateTimeField(auto_now_add=True, editable=False)
    date_update = models.DateTimeField(auto_now=True, auto_now_add=True, editable=False)
    ville = models.CharField(u"nom ville", null=True, max_length=200)
    @property
    def adresse_plain(self):
        a = re.sub(r'\r\n|\r|\n', ' ', strip_tags(self.adresse))

        return a

    def __unicode__(self):
        return self.nom
