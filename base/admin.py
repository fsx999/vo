# coding=utf-8

from djangocms_text_ckeditor.widgets import TextEditorWidget
from django.db import models
__author__ = 'paul'
from easy_maps.widgets import AddressWithMapWidget
from django.contrib import admin
from base.models import Professeur
from base.models import Salle
from django import forms

class SalleAdmin(admin.ModelAdmin):
    formfield_overrides = {
        models.TextField: {'widget': TextEditorWidget},
    }
    class form(forms.ModelForm):
        class Meta:
            widgets = {
                'adresse': TextEditorWidget
            }

admin.site.register(Professeur)
admin.site.register(Salle, SalleAdmin)
