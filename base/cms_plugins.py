# coding=utf-8
from cms.plugin_base import CMSPluginBase
from cms.plugin_pool import plugin_pool
from cmsplugin_filer_folder.cms_plugins import FilerFolderPlugin
from cmsplugin_filer_image.cms_plugins import FilerImagePlugin
from base.models import MonFilerImage, Professeur, ListeProfesseur, ListeSalle, Salle

__author__ = 'paul'


class MonFilerImagePlugin(FilerImagePlugin):
    model = MonFilerImage


plugin_pool.unregister_plugin(FilerImagePlugin)
plugin_pool.register_plugin(MonFilerImagePlugin)


class ProfesseurPlugin(CMSPluginBase):
    model = ListeProfesseur
    name = "Liste professeur"
    render_template = "base/professeur.html"

    def render(self, context, instance, placeholder):
        """Update the context with plugin's data"""
        professeurs = Professeur.objects.all()

        context.update({'professeurs': professeurs,
                        'object': instance,
                        'placeholder': placeholder})
        return context


class SallePlugin(CMSPluginBase):
    model = ListeSalle
    name = "Liste des salles"
    render_template = "base/salles.html"

    def render(self, context, instance, placeholder):
        """Update the context with plugin's data"""
        salles = Salle.objects.all().order_by('id')

        context.update({'salles': salles,
                        'object': instance,
                        'placeholder': placeholder})
        return context

plugin_pool.unregister_plugin(FilerFolderPlugin)
FilerFolderPlugin.text_enabled = True
plugin_pool.register_plugin(FilerFolderPlugin)
plugin_pool.register_plugin(ProfesseurPlugin)
plugin_pool.register_plugin(SallePlugin)
